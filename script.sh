#!/bin/bash

# 1) Identificar o usuário que tem o maior “size”; -ok
# 2) Ordenar o “username” em ordem alfabética. -ok
# 3) Identificar a quantidade de usuários que estão na seguinte faixa do INBOX: -ok
#   o Entre 25000 e 48999 msgs - ok
#   o Entre 49000 e 50000 msgs - ok
#   o Acima de 50001 msgs - ok

MAIOR_SIZE=0
USERS_FAIXA1=0
USERS_FAIXA2=0
USERS_FAIXA3=0
arr=()
I=0

while read LINHA; do

  USERNAME=$(echo $LINHA | awk '{print $1}')
  INBOX=$(echo $LINHA | awk '{print $3}')
  SIZE_ATUAL=$(echo $LINHA | awk '{print $5}')

  if [ "$SIZE_ATUAL" -gt "$MAIOR_SIZE" ]; then
    MAIOR_SIZE=$(echo $SIZE_ATUAL)
    USER_MAIOR_SIZE=$(echo $USERNAME)
  fi

  arr["$I"]="$USERNAME"
  let I=$I+1;

  if [ "$INBOX" -ge 25000 ] && [ "$INBOX" -le 48999 ]; then
    let USERS_FAIXA1=$USERS_FAIXA1+1;
  elif [ "$INBOX" -ge 49000 ] && [ "$INBOX" -le 50000 ]; then
    let USERS_FAIXA2=$USERS_FAIXA2+1;
  elif [ "$INBOX" -ge 50001 ]; then
    let USERS_FAIXA3=$USERS_FAIXA3+1;
  fi
  
done < input.txt

echo "$USER_MAIOR_SIZE" > user_maior_size
echo "$USERS_FAIXA1" > faixa_1
echo "$USERS_FAIXA2" > faixa_2
echo "$USERS_FAIXA3" > faixa_3

for el in "${arr[@]}"
do
    echo "$el"
done | sort > nomes_ordenados

