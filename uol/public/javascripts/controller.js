angular.module('uol').controller('MyCtrl', function($scope, upload, FileService, $http){
  $scope.uploadedFile = true;
  $scope.listar = true;
  // Upload a new document
  $scope.uploadFile = function() {
    $scope.arquivoUpload = this.files;
  }

  $scope.anexa = function() {
    if ($scope.arquivoUpload) { 
      FileService.uploadDocument($scope.arquivoUpload[0], 
        function(res) {
          if (res.error_code == 0) {
            // console.log(res);
            $scope.fileName = res.nome;
            FileService.uploadDocument2(res.nome,
              function(res) {
                $scope.bigTotalItems = res.length;
                $scope.uploadedFile = false;
              }, 
              function (erro) {
                console.log(erro)    
              }
            )  
          }
        }, 
        function(erro) {
          console.log(erro)
        }
      );
    }
  }

  $scope.buscaUsuario = function(nome) {
    FileService.searchUser(nome, $scope.fileName,
      function(res) {
        if (res.length > 0) {
          $scope.mensagem = "Nome: " + res[0].userName + ", Inbox: " + res[0].inbox + ", Size: " + res[0].size;
        }
        else {
         $scope.mensagem = "Usuário não encontrado."; 
        }
      }, 
      function(erro) {
        console.log(erro)
      }
    );
  }

  $scope.buscaInbox = function() {
    FileService.searchInbox($scope.fileName,
      function(res) {
          $scope.mensagem = "Nome: " + res;
      }, 
      function(erro) {
        console.log(erro)
      }
    );
  }

  $scope.buscaSize = function() {
    FileService.searchSize($scope.fileName,
      function(res) {
        $scope.mensagem = "Nome: " + res;
      }, 
      function(erro) {
        console.log(erro)
      }
    );
  }
  
  $scope.currentPage = 1;
  $scope.maxSize = 5;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
    console.log($scope.currentPage);
  };

  $scope.numPages = function () {
    return Math.ceil($scope.bigTotalItems / $scope.maxSize);
  };

  $scope.pageChanged = function() {
    $scope.listarUsuarios();
  };

  $scope.listarUsuarios = function() {
    $scope.listar = false;
    
    FileService.listUser($scope.fileName,$scope.maxSize,$scope.currentPage,
      function(res) {
        $scope.Users = res;
      }, 
      function(erro) {
        console.log(erro)
      }
    );
  };

});