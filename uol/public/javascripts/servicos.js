'use strict';
//serviços de autenticação e chamadas para a api
angular.module('uol')
.factory('FileService',['$http', 'NODE_SERVER', function($http, NODE_SERVER) {
    return {
      uploadDocument: function(data, success, error) {
        var fd = new FormData();
        fd.append('file', data);

        $http.post(NODE_SERVER + 'enviar-arquivo', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        }).success(success).error(error)
      },
      uploadDocument2: function(nome, success, error) {
        $http.get(NODE_SERVER + 'ler-arquivo/' + nome).success(success).error(error)
      },
      searchUser: function(nome, arquivo, success, error) {
        $http.get(NODE_SERVER + 'buscar-usuario/' + nome + '/' + arquivo).success(success).error(error)
      },
      searchInbox: function(arquivo, success, error) {
        $http.get(NODE_SERVER + 'maior-inbox/' + arquivo).success(success).error(error)
      },
      searchSize: function(arquivo, success, error) {
        $http.get(NODE_SERVER + 'maior-size/' + arquivo).success(success).error(error)
      },
      listUser: function(arquivo, item, numero, success, error) {
        $http.get(NODE_SERVER + 'usuarios/'+arquivo+'/'+item+'/'+numero).success(success).error(error)
      }
    } 
}])