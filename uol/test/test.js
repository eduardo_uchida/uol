var request = require('supertest');
var assert = require('assert');
// var config = require('config');
var app = require('../app');
var fs = require('fs');

describe('Rotas de Teste', function () {
  var nome='teste.txt';

  it('GET /ler-arquivo/:nome', function(done){
    request(app)
      .get('/ler-arquivo/teste.txt')
      .end(function(err, response) {
        var body = response.body;
        assert.equal(200, response.status);
        assert.deepEqual(body, 
          [{ userName: 'damejoxo@uol.com.br', inbox: 2200463, size: 2142222 },
          { userName: 'li_digik@uol.com.br', inbox: 11000230, size: 1032646 },
          { userName: 'yyfdinny@uol.com.br', inbox: 11105330, size: 11134606 },
          { userName: 'sihdtelu@uol.com.br', inbox: 10212210, size: 1033573 },
          { userName: 'vx.qka@uol.com.br', inbox: 1042150, size: 11113043 }]
        );
        done();
      })
  });

  it('GET /buscar-usuario/:usuario/:arquivo', function(done){
    request(app)
      .get('/buscar-usuario/sihdtelu@uol.com.br/teste.txt')
      .end(function(err, response) {
        var body = response.body;
        assert.equal(200, response.status);
        assert.deepEqual(body, 
          [{ userName: 'sihdtelu@uol.com.br', inbox: 10212210, size: 1033573 }]
        );
        done();
      })
  });

  it('GET /maior-inbox/:arquivo', function(done){
    request(app)
      .get('/maior-inbox/teste.txt')
      .end(function(err, response) {
        var body = response.body;
        assert.equal(200, response.status);
        assert.deepEqual(body, 'yyfdinny@uol.com.br');
        done();
      })
  });

  it('GET /maior-size/:arquivo', function(done){
    request(app)
      .get('/maior-size/teste.txt')
      .end(function(err, response) {
        var body = response.body;
        assert.equal(200, response.status);
        assert.deepEqual(body, 'yyfdinny@uol.com.br');
        done();
      })
  });
  
  it('GET /usuarios/:arquivo/:itens/:numero', function(done){
    request(app)
      .get('/usuarios/teste.txt/2/2')
      .end(function(err, response) {
        var body = response.body;
        assert.equal(200, response.status);
        assert.deepEqual(body, 
          [ 'yyfdinny@uol.com.br', 'sihdtelu@uol.com.br' ]
        );
        done();
      })
  });

});