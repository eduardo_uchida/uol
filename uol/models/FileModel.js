'use strict'

function FileDAO() {
  this.results = [];
}

//Model para pegar todos os mapas carregados
FileDAO.prototype.uploadDocument = function(arquivo, callback) {
  var fs = require('fs');

  fs.readFile('uploads/'+arquivo, function ( err, logData ) {
    if ( err ) {
      callback(err, null);
    } else {
      var text = logData.toString();
      var results = {};
      var lines = text.split( '\n' );    
      var results = [];
      lines.forEach(function ( line ) {
          var parts = line.split( ' ' );
          var obj = {};
          obj.userName = parts[ 0 ];
          obj.inbox = parseInt( parts[ 2 ] );
          obj.size = parseInt( parts[ 4 ] );
          
          results.push(obj);
      });
      FileDAO.results = results;
      callback(null, FileDAO.results );
    }
  });
};

function lerArquivo(arquivo) {
  var fs = require('fs');

  fs.readFile('uploads/'+arquivo, function ( err, logData ) {
    if ( err ) {
      callback(err, null);
    } else {
      var text = logData.toString();
      var results = {};
      var lines = text.split( '\n' );    
      var results = [];
      lines.forEach(function ( line ) {
          var parts = line.split( ' ' );
          var obj = {};
          obj.userName = parts[ 0 ];
          obj.inbox = parseInt( parts[ 2 ] );
          obj.size = parseInt( parts[ 4 ] );
          
          results.push(obj);
      });
      FileDAO.results = results;
    }
  });
}

//Model para buscar um usuário
FileDAO.prototype.searchUser = function(usuario, arquivo, callback) {
  lerArquivo(arquivo);
  if (FileDAO.results.length > 0) {
    function usr (value) {
      if (value.userName == usuario) {
        return value;
      }
    }
    var arr = FileDAO.results.filter(usr);
    
    callback(null, arr);  
  } else {
    callback(err, null);
  }
};

//Model para pegar maior inbox
FileDAO.prototype.biggestInbox = function(arquivo, callback) {
  lerArquivo(arquivo);
  if (FileDAO.results.length > 0) {
    var biggest = FileDAO.results[0].inbox;
    var usr = "";
    for (var i = 0; i < FileDAO.results.length; i++) {
      if (FileDAO.results[i].inbox > biggest) {
        biggest = FileDAO.results[i].size;
        usr = FileDAO.results[i].userName;
      }
    }

    callback(null, usr);  
  } else {
    callback(err, null);
  }
  
};

//Model para identificar o usuário que tem o maior “size”
FileDAO.prototype.biggestSize = function(arquivo, callback) {
  lerArquivo(arquivo);
  if (FileDAO.results.length > 0) {
    var biggest = FileDAO.results[0].size;
    var usr = "";
    
    for (var i = 0; i < FileDAO.results.length; i++) {
      if (FileDAO.results[i].size > biggest) {
        biggest = FileDAO.results[i].size;
        usr = FileDAO.results[i].userName;
      }
    }

    callback(null, usr);  
  } else {
    callback(err, null);
  }
};

//Model para listar todos os usuários, com o backend suportando paginação.
FileDAO.prototype.allUsers = function(arquivo, itens, numero, callback) {
  lerArquivo(arquivo);  
  
  var posicao = (numero - 1) * itens;

  if (FileDAO.results.length > 0) {
    var usr = []
    if ((FileDAO.results.length - 1) > posicao ) {
      for (var i=0; i < itens; i++) {
        usr.push(FileDAO.results[posicao].userName);
        posicao = posicao + 1;
      };
    } else {
      var resto = FileDAO.results.length - posicao;
      for (var i=0; i < resto; i++) {
        usr.push(FileDAO.results[posicao].userName);
        posicao = posicao + 1;
      };
    }
    callback(null, usr);
  } else {
   callback(err, null); 
  }
};


module.exports = new FileDAO();