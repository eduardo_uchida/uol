var debug = require('debug')('uol:controller');
function FileController(FileModel) {
  this.model = FileModel;
}

//Controller para upload de arquivo
FileController.prototype.uploadDocument = function(req, res, next) { 
  this.model.uploadDocument(req.params.nome,function(err, data) {
    if(err) {
      console.log(err);
      return next(err);
    }
    res.json(data);
  });
};

//Controller para buscar usuário
FileController.prototype.searchUser = function(req, res, next) {  
  var usuario = req.params.usuario;
  var arquivo = req.params.arquivo;
  this.model.searchUser(usuario, arquivo, function(err, data) {
    if(err) {
      console.log(err);
      return next(err);
    }
    res.json(data);
  });
};

//Controller para buscar maio inbox
FileController.prototype.biggestInbox = function(req, res, next) {  
  var arquivo = req.params.arquivo;
  this.model.biggestInbox(arquivo, function(err, data) {
    if(err) {
      console.log(err);
      return next(err);
    }
    res.json(data);
  });
};

//Controller para identificar o usuário que tem o maior “size”
FileController.prototype.biggestSize = function(req, res, next) {  
  var arquivo = req.params.arquivo;
  this.model.biggestSize(arquivo, function(err, data) {
    if(err) {
      console.log(err);
      return next(err);
    }
    res.json(data);
  });
};

//Controller para listar todos os usuários, com o backend suportando paginação.
FileController.prototype.allUsers = function(req, res, next) {  
  var arquivo = req.params.arquivo;
  var itens = req.params.itens;
  var numero = req.params.numero;

  this.model.allUsers(arquivo, itens, numero, function(err, data) {
    if(err) {
      console.log(err);
      return next(err);
    }
    res.json(data);
  });
};

module.exports = function(FileModel) {
  return new FileController(FileModel);
};