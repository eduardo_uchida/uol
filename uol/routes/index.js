var express = require('express');
var router = express.Router();
var name = "";

// var multer  = require('multer');
// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//       cb(null, './uploads/')
//   },
//   filename: function (req, file, cb) {
//       cb(null, file.originalname+Date.now());
//       name = file.originalname;
//   }
// });

// var upload = multer({ storage: storage });

var FileModel     = require('../models/FileModel');
var FileController  = require('../controllers/FileController')(FileModel);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/ler-arquivo/:nome', FileController.uploadDocument.bind(FileController));
// router.post('/enviar-foto', FileController.uploadPhoto.bind(FileController));

//rota para buscar um usuário
router.get('/buscar-usuario/:usuario/:arquivo', FileController.searchUser.bind(FileController));

//rota para buscar um maior inbox
router.get('/maior-inbox/:arquivo', FileController.biggestInbox.bind(FileController));

//rota para identificar o usuário que tem o maior “size”
router.get('/maior-size/:arquivo', FileController.biggestSize.bind(FileController));

//rota para listar todos os usuários, com o backend suportando paginação.
router.get('/usuarios/:arquivo/:itens/:numero', FileController.allUsers.bind(FileController));

module.exports = router;
