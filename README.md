# README #
* Esse projeto foi desenvolvido por Eduardo Massami Uchida, como um teste para o UOL.
* Verão 0.0.1
* A tecnologia utilizada foi o NODEJS com front-end desenvolvido em AngularJS, decidiu-se por tal pela velocidade alcançada com a arquitetura assíncrona do NODEJS e a facilidade de se trabalhar com Javascript tanto no front como no backend.


### Subindo o Projeto ###
**Configuração**

* Para subir o projeto será necessário instalar o NPM e o NODE, segue link com instruções para tal [http://nodebr.com/instalando-node-js-atraves-do-gerenciador-de-pacotes/](Link URL).
Com o NODE e o NPM instalados digite em um terminal **npm install -g nodemon**, clone o projeto, entre na pasta **cd uol/uol** e digite o seguinte comando **npm install**.

Ao final da instalação digite **nodemon** no seu terminal e a aplicação estará pronta para uso.

**Dependências**

* As depencias do projeto estão listadas no package.json

**Testes**

* Para rodar testes, vá para o terminal, acesse o diretório (**cd uol/uol**) do projeto e digite **npm test**.

**Arquivo Bash**

* Para rodar o arquivo bash, pelo terminal acesse **cd uol/** e digite **chmod -x script.sh**, após isso digite **./script.sh**, após o processamento 4 arquivo serão criados com lista de usuários ordenados, usuário com maior size e faixas de inbox (25000 e 48999 mensagens, 49000 e 50000 mensagens e acima de 50001 mensagens).

### Funcionamento ###
O servidor tem 4 rotas criadas:


____________________________________________________________
**http://localhost:3000/enviar-arquivo/**

Função: Rota para enviar um arquivo.

Verbo HTTP: POST

Parâmetro: Nenhum


____________________________________________________________
**http://localhost:3000/buscar-usuario/:nome/:arquivo**

Função: Rota para devolver um usuário específico.

Verbo HTTP: GET

Parâmetro: o nome do usuário (:nome) e nome do arquivo (:arquivo *vem com o retorno de **http://localhost:3000/enviar-arquivo/**)  


____________________________________________________________
**http://localhost:3000/maior-inbox/:arquivo**

Função: Rota para saber o maior inbox.

Verbo HTTP: GET

Parâmetro: nome do arquivo (:arquivo *vem com o retorno de **http://localhost:3000/enviar-arquivo/**) 


____________________________________________________________
**http://localhost:3000/maior-size/:arquivo**

Função: Rota para saber o maior size.

Verbo HTTP: GET

Parâmetro: nome do arquivo (:arquivo *vem com o retorno de **http://localhost:3000/enviar-arquivo/**)
 

____________________________________________________________
**http://localhost:3000/usuarios/:arquivo/:item/:numero**

Função: Rota exibir usuários com paginação.

Verbo HTTP: GET

Parâmetro: nome do arquivo (:arquivo *vem com o retorno de **http://localhost:3000/enviar-arquivo/**), itens por página (ex.: 5), número da página (ex.: 6).
 

____________________________________________________________
### Contato ###
* Eduardo Massami Uchida

email: massami.uchida@gmail.com

tel: (11) 98423-3057